#include "Carro.h"
#include "Pista.h"
#include "FilaEnc.h"
#include "Relogio.h"
#include "ListaEnc.h"
#include "Elemento.h"
#include "Evento.h"
#include "EventoNovoCarro.h"
#include "EventoMudancaSemaforo.h"
#include "EventoTrocaDePista.h"
#include "EvenetoCarroNoSemaforo.h"
#include "Semaforo.h"

using std::cin;
using std::cout;

int const maxTempo, nTempo, sTempo, lTempo, oTempo;
Pista **pistas = new Pista*[14]; 
Relogio *clock = new Relogio();

int main(){

void recebeEntradas() {
	cout << "Qual o tempo da simulação?";
	cin >> maxTempo;
	cout << "Qual o tempo do semaforo NORTE?";
	cin >> nTempo;
	cout << "Qual o tempo do semaforo SUL?";
	cin >> sTempo;
	cout << "Qual o tempo do semaforo OESTE?";
	cin >> oTempo;
	cout << "Qual o tempo do semaforo LESTE?";
	cin >> lTempo;
}

void inicializaPistas() {
	//Pista(int _velocidade, int _tamanho, int _id, 
	//    bool _sumidouro, bool _fonte, int _frequencia, int _variancia) 

	pistas[0] = new Pista(80, 2000, 0, true, false, 0, 0); //sumidouro
	pistas[1] = new Pista(80, 2000, 1, false, true, 10, 2); // fonte
	pistas[2] = new Pista(60, 500, 2, false, true, 20, 5);
	pistas[3] = new Pista(60, 500, 3, true, false, 0, 0);
	pistas[4] = new Pista(60, 500, 4, true, false, 0, 0);
	pistas[5] = new Pista(60, 500, 5, false, true, 30, 7);
	pistas[6] = new Pista(60, 300, 6, false, false, 0, 0);
	pistas[7] = new Pista(60, 300, 7, false, false, 0, 0);
	pistas[8] = new Pista(40, 500, 8, false, true, 20, 5);
	pistas[9] = new Pista(40, 500, 9, true, false, 0, 0);
	pistas[10] = new Pista(40, 500, 10, true, false, 0, 0);
	pistas[11] = new Pista(40, 500, 11, false, true, 60, 15);
	pistas[12] = new Pista(30, 400, 12, false, true, 10, 2);
	pistas[13] = new Pista(30, 400, 13, true, false, 0, 0);
}

void criaSemaforo() {
	Semaforo semaforo = new Semaforo(nTempo, sTempo, lTempo, oTempo);
}

void iniciaEventos() {
	clock->adicionaNoInicio(EventoMudancaSemaforo(clock, semaforo));

}

void imprimeDados() {

} 



int main() {

	recebeEntradas();
	inicializaPistas();
	criaSemaforo();
	inciaEventos();

	while(clock->retiraDoInicio()->getTempo() <= maxTempo) {
		clock->retiraDoInicio()->executa();
	}
	imprimeDados();

}

	return 0;
}