#ifndef	CARRO_H
#define CARRO_H

class Carro {
	public:
		Carro(int id_destino){
			srand(time(NULL));
			actual_size = ((rand%8)+2);

			destino = id_destino;
		}
		
		~Carro(){}

		getSize(){
			return total_size;
		}
	private:
		int actual_size;						// tamanho do carro, de 2 a 10 metros.
		int total_size = actual_size + 3;		// tamanho total, contando carro, 2 metros para tras e um para frente.
		int destino = id_destino;
};

#endif