#ifndef	NOVOCARRO_H
#define NOVOCARRO_H


class Carro;
class Pista;

class EventoNovoCarro: private Evento {
	public:

		EventoNovoCarro(Pista* _lugar) {
			lugar = _lugar;
		}

		~EventoNovoCarro() {}

		int getTempo() {
			return tempo;
		}

		void executa() {
			Carro c = new Carro(id_destino)
			if(lugar->getEspacoLivre() > c.getSize()){
				lugar->adicionaCarro(c);
			} else {
				throw "Pista cheia, carro não adicionado";
			} 
			int id = lugar->getId();
			int id_destino;
			int id_sorteado;
			switch(id) {
				case 0:
					// pista = O1oeste
					break;
				case 1: 
					id_sorteado = (int)rand()%10;
					if (id_sorteado >= 2) {
						id_destino = 7; //C1leste
					} else if (id_sorteado == 1) {
						id_destino = 3; //N1norte
					} else {
						id_destino = 4; // S1sul
					}
					// O1leste	
					break;
				case 2:
					id_sorteado = (int)rand()%10;
					if (id_sorteado >= 2) {
						id_destino = 7; //C1leste 
					} else if(id_sorteado == 1) {
						id_destino = 0; //O1oeste
					} else {
						id_destino = 4; //S1sul 
					}
					// N1sul
					break;
				case 3:
					//N1norte		
					break;
				case 4:
					//S1sul		
					break;
				case 5:
					id_sorteado = (int)rand()%10;
					if (id_sorteado >=2) {
						id_destino = 7; //C1leste
					} else if (id_sorteado == 1) {
						id_destino = 3; //N1norte
					} else {
						id_destino = 0; //O1oeste
					}
					//S1norte
					break;
				case 6:
					id_sorteado = (int)rand()%10;
					if (id_sorteado <= 4) {
						id_destino = 0; //O1oeste
					} else if (id_sorteado > 4 && id_sorteado <= 7) {
						id_destino = 3; //N1norte
					} else {
						id_destino = 4; //S1sul
					}
					//C1oeste
					break;
				case 7:
					id_sorteado = (int)rand()%10;
					if (id_sorteado <= 4) {
						id_destino = 13; //L1leste
					} else if (id_sorteado > 4 && id_sorteado <= 7) {
						id_destino = 9; //N2norte
					} else {
						id_destino = 10; //S2sul
					}
					//C1leste	
					break;
				case 8:
					id_sorteado = (int)rand()%10;
					if (id_sorteado <= 4) {
						id_destino = 13; //L1leste
					} else if (id_sorteado > 4 && id_sorteado <= 7) {
						id_destino = 3; //N1norte
					} else {
						id_destino = 4; //S1sul
					}
					//N2sul		
					break;
				case 9:
					//N2norte		
					break;
				case 10:
					//S2sul
					break;
				case 11:
					id_sorteado = (int)rand()%10;
					if (id_sorteado <= 4) {
						id_destino = 13; //L1leste
					} else if (id_sorteado > 4 && id_sorteado <= 7) {
						id_destino = 10; //S2sul
					} else {
						id_destino = 6; //C1oeste
					}
					//S2norte
					break;
				case 12:
					id_sorteado = (int)rand()%10;
					if (id_sorteado <= 4) {
						id_destino = 9; //N2norte
					} else if (id_sorteado > 4 && id_sorteado <= 7) {
						id_destino = 7; //C1leste
					} else {
						id_destino = 10; //S2sul
					}
					//L1oeste
					break;
				case 13:
					//L1leste
					break;

			}
		}

	private:
		Pista* lugar;
		//int tempo;

};

#endif