// copyright 2015 ninguem

#ifndef ELEMENTO_HPP
#define ELEMENTO_HPP

template<typename T>
class Elemento {
 private:
	T *info;  //! O dado contido pelo elemento
	Elemento<T>* _next;  //! O próximo elemento da fila
 public:
    //! Construtor padrão. Inicializa info e _next
	Elemento(const T& info, Elemento<T>* next) : info(new T(info)), _next(next) {}
    //! Destrutor padrão. Libera info.
	~Elemento() {
		delete info;
	}
    //! getProximo(): retorna _next;
	Elemento<T>* getProximo() const {
		return _next;
	}
    //! getInfo(): retorna o valor armazenado pelo elemento
	T getInfo() const {
		return *info;
	}
    //! setProximo(): altera o próximo elemento da lista
	void setProximo(Elemento<T>* next) {
		_next = next;
	}
};

#endif

