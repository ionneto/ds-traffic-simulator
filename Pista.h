#ifndef PISTA_H
#define PISTA_H

class Carro;

class Pista : public FilaEnc<Carro> {
	public:

		Pista(int _velocidade, int _tamanho, int _id, bool _sumidouro, 
			bool _fonte, int _frequencia, int _variancia) {
			velocidade = _velocidade;
			tamanho = _tamanho;
			id = _id;
			sumidouro = _sumidouro;
			fonte = _fonte;
			frequencia = _frequencia;
			variancia = _variancia;
		}

		~Pista() {

		}

		void adicionaCarro(Carro c) {
			this.inclui(c);
			espacoLivre = espacoLivre - c->getSize();
		}

		int getEspacoLivre() {
			return espacoLivre;
		}

		int getVelocidade() {
			return velocidade;
		}

		int getTamanho() {
			return tamanho;
		}

		bool eSumidouro() {
			return sumidouro;
		}

		bool eFonte() {
			return fonte;
		}
		int getId() {
			return id;
		}

	private:
		int espacoLivre;
		int velocidade;
		int tamanho;
		int id;
		int frequencia;
		int variancia;
		bool sumidouro;
		bool fonte;
};

#endif