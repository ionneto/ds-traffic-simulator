#ifndef FILA_H
#define FILA_H
#include "Elemento.hpp"

template <typename T>
class FilaEnc{
 private:
    Elemento<T> * inicio;
    Elemento<T> * fim;
    int size;

 public:
	FilaEnc<T>(): inicio(NULL), fim(NULL), size(0) {}
	~FilaEnc() {
	    limparFila();
	}
	void inclui(const T& dado) {
	    Elemento<T> * novo;
	    novo = new Elemento<T>(dado, NULL);
	    if (novo == NULL) {
	        throw "Failed to allocate memory";
	    } else {
	        if (filaVazia()) {
	            inicio = novo;
	        } else {
	            fim->setProximo(novo);
	        }
	        fim = novo;
	        size++; // increases the size variable by 1 unit
	    }
	}
	T retira() {
	    Elemento<T> * sai;
	    T retorno;
	    if (filaVazia()) {
	        throw "Empty FIFO";
	    } else {
	        sai = inicio;
	        retorno = sai->getInfo();
	        inicio = sai->getProximo();
	        if (size == 1) {
	            fim = NULL;
	        }
	        size--;
	    }
	    delete sai;
	    return retorno;
	}
	T ultimo() {
	    if (filaVazia()) {
	        throw "Fila Vazia";
	    } else {
	        return fim->getInfo();
	    }
	}
	T primeiro() {
	    if (filaVazia()) {
	        throw "Fila Vazia";
	    } else {
	        return inicio->getInfo();
	    }
	}
	bool filaVazia() {
	    return size == 0;
	}
	void limparFila() {
	    Elemento<T> *atual, *anterior;
	    if (!filaVazia()) {
	        atual = fim;
	        while (atual != NULL) {
	            anterior = atual;
	            atual = atual->getProximo();
	            delete anterior;
	        }
	    }
	    size = 0;
	}
};

#endif
