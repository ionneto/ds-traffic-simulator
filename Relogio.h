#ifndef RELOGIO_H
#define RELOGIO_H

class Relogio: public ListaEnc<Evento>{
	public:
		Relogio(){
			head = NULL;
			size = 0;
		}
		
		~Relogio(){}

		
		void adicionaEvento(Evento e) {
		    if (listaVazia()) {
		        adicionaNoInicio(e);
		    }
		    Elemento<T> * atual;
		    int pos;
		    atual = head;
		    pos = 0;
		    while (atual->getProximo() != NULL && maior(e, atual->getInfo())) {
		        atual = atual->getProximo();
		        pos++;
		    }
		    if (maior(e, atual->getInfo())) {
		        return adicionaNaPosicao(e, pos + 1);
		    }
		    return adicionaNaPosicao(e, pos);
			
		}

		

		bool maior(Evento* a, Evento* b) {
			return a->getTempo() > b->getTempo();
		}
		bool menor(Evento* a, Evento* b) {
			return a->getTempo() < b->getTempo();
		}
		bool igual(Evento* a, Evento* b) {
			return a->getTempo() == b->getTempo();
		}

	private:

};



#endif