// Copyright [2015] <Ion>
#include "Elemento.hpp"

template<typename T>
class ListaEnc {
 public:
// Cria a lista encadeda
	ListaEnc() {
	    head = NULL;
	    size = 0;
	}
	~ListaEnc() {
	
	}
	// inicio

    // adiciona um elemento no inicio da lista
	void adicionaNoInicio(const T& dado) {
	    Elemento<T> * novo;
	    novo = new Elemento<T>(dado, head);
	    if (novo == NULL) {
	        throw 20;
	    } else {
	    head = novo;
	    size++;
	    }
	}
// retira elemento do inicio da lista
	T retiraDoInicio() {
	    if (listaVazia()) {
	        throw 20;
	    } else {
    	    Elemento<T> * saiu;
    	    saiu = head;
    	    T volta = saiu->getInfo();
    	    head = saiu->getProximo();
    	    delete saiu;
    	    size--;
    	    return volta;
	    }
	}
// apaga o elemento do inicio da lista
	void eliminaDoInicio() {
	    if (listaVazia()) {
	        throw 20;
	    } else {
	        Elemento<T> * sai;
	        sai = head;
	        head = head->getProximo();
	        delete sai;
	        size--;
	    }
	}
// adiciona um elemento na posicao pos
	void adicionaNaPosicao(const T& dado, int pos) {
	    Elemento<T> * adicionado, * anterior;
	    if (pos > size) {
            throw 20;
        }
        if (pos == 0) {
            adicionaNoInicio(dado);
            return;
        }
        adicionado = new Elemento<T>(dado, NULL);
        if (adicionado == NULL) {
            throw 20;
        }
        anterior = head;
        for (int i = 0; i < pos - 1; i++) {
            anterior = anterior->getProximo();
        }
        adicionado->setProximo(anterior->getProximo());
        adicionado = new Elemento<T>(dado, anterior->getProximo());
        anterior->setProximo(adicionado);
        size++;
	}
// retorna a posição do dado
	int posicao(const T& dado) const {
	    if (listaVazia()) {
	        throw 20;
	    } else {
	        Elemento<T> * compara;
	        compara = head;
	        for (int c = 0; c < size; c++) {
	            if (compara->getInfo() == dado) {
	                return c;
	                }
	            compara = compara->getProximo();
	        }
	       throw 20;
	    }
	}
// retorna o que esta no ponteiro T na posicao do dado
	T* posicaoMem(const T& dado) const {
	if (listaVazia()) {
	    throw 20;
	    } else {
	        Elemento<T> *procura = head;
	        for (int i = 0; i < size +1; i++) {
	            if (procura->getInfo() == dado) {
	                return &procura->getInfo();
	            }
	        procura = procura->getProximo();
	        }
	        throw 20;
	    }
	}

// retorna um booleano dizendo se  dado esta na lista
	bool contem(const T& dado) {
	    if (listaVazia()) {
	        throw 20;
	    } else {
	        Elemento<T> * procura = head;
	        for (int i = 0; i < size; i++) {
	            if (procura->getInfo() == dado) {
	                return true;
	            }
	            procura = procura->getProximo();
	        }
        return false;
	    }
	}
// retira um dado da posicao especificada
	T retiraDaPosicao(int pos) {
	    Elemento<T> * anterior, * sai;
	    T retorno;
	     if (listaVazia()) {
	        throw 20;
	    }
	     if (pos == 0) {
	        return retiraDoInicio();
	    }
	    if (pos >= size) {
            throw 20;
	    }
	    anterior = head;
	    for (int i = 0; i < pos -1; i++) {
	        anterior = anterior->getProximo();
	    }
	    sai = anterior->getProximo();
	    retorno = sai->getInfo();
	    anterior->setProximo(sai->getProximo());
	    size--;
	    delete sai;
	    return retorno;
    }
// adiciona um dado ao fim da lista
	void adiciona(const T& dado) {
	    adicionaNaPosicao(dado, size);}
// retira do fim da lista
	T retira() {
	if (listaVazia()) {
	        throw 20;
	    } else {
	        return retiraDaPosicao(size-1);
	    }
	}
// retira um elemento especifico da lista
	T retiraEspecifico(const T& dado) {
	    if (listaVazia()) {
	        throw 20;
	    } else {
	        Elemento<T> *procura = head;
	        T temp;
	        for (int i = 0; i < size; i++) {
	            if (procura->getInfo() == dado) {
	                temp = procura->getInfo();
	                retiraDaPosicao(i);
	                return temp;
	            }
                procura = procura->getProximo();
	        }
        throw 20;
	    }
	}
// adiciona o elemento, em uma lista ordenada, no seu lugar correto
	void adicionaEmOrdem(const T& data) {
	    if (listaVazia()) {
	        adicionaNoInicio(data);
	    }
	    Elemento<T> * atual;
	    int pos;
	    atual = head;
	    pos = 0;
	    while (atual->getProximo() != NULL && maior(data, atual->getInfo())) {
	        atual = atual->getProximo();
	        pos++;
	    }
	    if (maior(data, atual->getInfo())) {
	        return adicionaNaPosicao(data, pos + 1);
	    }
	    return adicionaNaPosicao(data, pos);
	}
// diz se a lista esta vazia
	bool listaVazia() const {
	    return size == 0;
	}
// diz se dado1 = dado2
	bool igual(T dado1, T dado2) {
	    return dado1 == dado2;
	}
// diz se dado1>dado2
	bool maior(T dado1, T dado2) {
	    return dado1 > dado2;
	}
// diz se dado1>dado2
	bool menor(T dado1, T dado2) {
	    return dado1 < dado2;
	}
// destroi a lista
	void destroiLista() {
	     if (listaVazia()) {
	        delete head;
	    }
	    Elemento<T> * aux;
	    Elemento<T> * anterior;
	    aux = head;
	    for (int i = 0; i < size - 1; i++) {
	        anterior = aux;
	        aux = aux->getProximo();
            anterior->~Elemento();
	    }
	    delete head;
	    size = 0;
	 }

 private:
	Elemento<T>* head;
	int size;
};