#ifndef SEMAFORO_H
#define SEMAFORO_H

class Semaforo{
public:
    bool abertoN = false;
    bool abertoS = false;
    bool abertoL = false;
    bool abertoO = false;
    int const tempoSemNorte, tempoSemSul, tempoSemLeste, tempoSemOeste;  // Tempos em segundos
 
    // Recebe tempo em minutos e armazena em segundos.
    Semaforo(int _tempoSemNorte, int _tempoSemSul,
     int _tempoSemLeste, int _tempoSemOeste)
    {
        tempoSemNorte = _tempoSemNorte;
        tempoSemSul = _tempoSemSul
        tempoSemLeste = _tempoSemLeste;
        tempoSemOeste = _tempoSemOeste;
        
    }
};

#endif // SEMAFORO_H
